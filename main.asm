%define ABI_CONSTANT 16
%define MAX_LENGTH_STRING 256		; 255 chars and null-terminator
%define POINTER_SIZE_IN_BYTES 8
%define NOT_FOUND_ERROR_CODE 1
%define TOO_LONG_ERROR_CODE 2

%include "words.inc"

not_found_error_message: db "key not found", 0
too_long_error_message: db "key is too long", 0
input_string: db "", 0

section .text
%include "lib.inc"
%include "dict.inc"
	
global _start
_start:
	sub rsp, ABI_CONSTANT	
	mov [rsp], r13
	
	mov r13, first_word		; cur element in dictionary

	mov rdi, input_string
	mov rsi, MAX_LENGTH_STRING
	call read_line	; -> rax, rdx
	test rax, rax
	jz .too_long
	
	mov rdi, rax
	mov rsi, r13
	call find_word	; -> rax
	test rax, rax
	jz .not_found
	
	jmp .good
	
	.too_long:
		mov rdi, too_long_error_message
		call print_error
		mov rdi, TOO_LONG_ERROR_CODE
		jmp .end
	
	.not_found:
		mov rdi, not_found_error_message
		call print_error
		mov rdi, NOT_FOUND_ERROR_CODE
		jmp .end
		
	.good:
		mov r13, rax
		
		mov rdi, r13
		call string_length				; -> rax
		add r13, rax	
		inc r13							; next key pointer
			
		add r13, POINTER_SIZE_IN_BYTES	; value
		
		mov rdi, r13
		call print_string				; print value
		
		xor rdi, rdi
		
	.end:
		mov r13, [rsp]
		add r13, ABI_CONSTANT
		call exit
