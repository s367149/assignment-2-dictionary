global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_uint
global print_int
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_equals
global string_copy

%define ABI_CONSTANT 16
%define STDIN 0
%define STDOUT 1
%define STDERR 2

section .text
 
; Принимает код возврата (rdi) и завершает текущий процесс.
exit: 
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку (rdi), возвращает её длину (rax).
; Нуль-терминатор не входит в длину строки!
string_length:
	xor eax, eax
	.loop:
		cmp [rdi + rax], byte 0
		jz .end
		inc rax
			jmp .loop
	.end:
		ret

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stdout.
print_string:
	mov rsi, STDOUT

; Принимает указатель на нуль-терминированную строку (rdi)
; и дескриптор (rsi), выводит строку в дескриптор (stdout/stderr).
abstract_print:
	push rdi
	push rsi
	call string_length  ; length -> rax
	pop rdi				; stdout descriptor (rsi -> rdi)
	pop rsi				; string address (rdi -> rsi)
	mov rdx, rax 		; string length in bytes
	mov rax, 1 			; write syscall number
	syscall
	ret

; Принимает указатель на нуль-терминированную строку (rdi), выводит её в stderr.
print_error:
	mov rsi, STDERR
	jmp abstract_print
	
; Переводит строку (выводит символ с кодом 0xA=10).
print_newline:
	mov rdi, `\n`

; Принимает код символа (rdi) и выводит его в stdout.
print_char: 
	sub rsp, ABI_CONSTANT
	mov [rsp], dil		; save char in stack
	mov rax, 1 			; write syscall number
	mov rsi, rsp 		; string address
	mov rdi, STDOUT 	; stdout descriptor 
	mov rdx, 1  		; string length in bytes
	syscall
	add rsp, ABI_CONSTANT
	ret

; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате.
; Совет: выделите место в стеке и храните там результаты деления.
; Не забудьте перевести цифры в их ASCII коды.
; Незначащие нули выводить не надо! Те если число 0, то вывод строго один 0.
print_uint:  
	push r12
	mov r12, 19		; shift
	mov rax, rdi	; current number
	xor rcx, rcx  	; cur index
	mov r8, 20  	; length in digits
	mov r9, 10		; its base
	sub rsp, 32		; 20 digits and null-terminator (21), and 11 bytes because abi
	.loop:
		cmp rcx, r8
		jz .end
		xor rdx, rdx
		div r9  	;result -> rax, remainder -> rdx
		add rdx, '0'
		mov rsi, 19
		sub rsi, rcx  ;rsi = 19 - rcx	
		mov [rsp + rsi], dl	
		cmp dl, '0'
		jz .end_iteration
		mov r12, rsi
		.end_iteration:
			inc rcx
			jmp .loop
	.end:
		mov [rsp + rcx], byte 0 
		add r12, rsp
		mov rdi, r12
		call print_string
		add rsp, 32
		pop r12
		ret

; Выводит знаковое 8-байтовое число (rdi) в десятичном формате.
; Незначащие нули выводить не надо! Те если число 0, то вывод строго один 0.
print_int:  
	xor eax, eax
	test rdi, rdi
	jl .negative
	.positive:
		jmp print_uint
	.negative:
		sub rsp, ABI_CONSTANT
		mov [rsp], rdi
		mov rdi, '-'
		call print_char
		mov rdi, [rsp]
		add rsp, ABI_CONSTANT
		neg rdi
		jmp print_uint

; Читает один символ из stdin и возвращает его (rax). Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, ABI_CONSTANT
	mov [rsp], byte 0	; push 0
	mov rax, 0		 	; read syscall number
	mov rdi, STDIN 		; stdin descriptor
	mov rsi, rsp		; char addres
	mov rdx, 1 			; string length (1) in bytes
	syscall
	xor eax, eax
	mov al, [rsp]
	add rsp, ABI_CONSTANT
	ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi). 
; В размер буфера входит нуль-терминатор!
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx. 
; В длину слова не входит нуль-терминатор!
; При неудаче возвращает 0 в rax.
; Эта функция должна дописывать к слову нуль-терминатор.
read_word: 
	push r12		; store callee-saved registers
	push r13
	push r14
	push rbx
	mov r12, rdi	; buffer address
	mov r13, rsi 	; buffer length
	xor r14, r14  	; word length
	xor rbx, rbx   	; flag
	.loop:
		cmp r14, r13
		jge .bad  		; r14 >= r13 -> .bad
		call read_char  ; char -> rax 
		cmp al, ` `  	; space
		jz .end_iteration
		cmp al, `\t`	; tab
		jz .end_iteration
		cmp al, `\n`  	; newline
		jz .end_iteration
		test al, al
		jz .good
		mov [r12 + r14], al
		inc r14
		mov rbx, 1
		jmp .loop	
		.end_iteration:
			cmp rbx, 1  ; if flag -> good
			jz .good
			jmp .loop
	.good:
		mov [r12 + r14], byte 0
		mov rax, r12
		mov rdx, r14
		jmp .end
	.bad:
	   	xor eax, eax
	.end:
		pop rbx
		pop r14
		pop r13
		pop r12
		ret
 
; Принимает: адрес начала буфера (rdi), размер буфера (rsi). 
; В размер буфера входит нуль-терминатор!
; Читает в буфер строку (до переноса строки) из stdin.
; Останавливается и возвращает 0 если строка слишком большая для буфера
; При успехе возвращает адрес буфера в rax, длину строки в rdx. 
; В длину строки не входит нуль-терминатор!
; При неудаче возвращает 0 в rax.
; Эта функция должна дописывать к строке нуль-терминатор.
read_line: 
	sub rsp, 8		; because abi (8+8+8+8=32)
	push r12		; store callee-saved registers
	push r13
	push r14
	mov r12, rdi	; buffer address
	mov r13, rsi 	; buffer length
	xor r14, r14  	; string length
	.loop:
		cmp r14, r13
		jge .bad  		; r14 >= r13 -> .bad
		
		call read_char  ; char -> rax 
		cmp al, `\n`  	; newline
		jz .good
		
		test al, al		; null-terminator
		jz .good
		
		mov [r12 + r14], al
		inc r14
		jmp .loop	
	.good:
		mov [r12 + r14], byte 0
		mov rax, r12
		mov rdx, r14
		jmp .end
	.bad:
	   	xor eax, eax
	.end:
		pop r14
		pop r13
		pop r12
		add rsp, 8
		ret
		
; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах.
; Незначащие нули в длину не входят!
; rdx = 0 если число прочитать не удалось.
parse_uint:
	xor eax, eax 	; number
	xor rcx, rcx 	; cur length
	push r12		; flag
	push r13		; flag
	push r14		; number length
	xor r12, r12	; store callee-saved registers
	xor r13, r13
	xor r14, r14  	
	mov r8, 10		; its base
	.loop:
		cmp [rdi + rcx], byte '0'
		jb .check_end	
		cmp [rdi + rcx], byte '9'
		ja .check_end
		cmp [rdi + rcx], byte '0'
		jnz .default
		.check_zero:
			mov r13, 1
			test r12, r12
			jz .end_iteration
		.default:
			xor r13, r13
			mov r12, 1
			mul r8  		;rax=rax*10
			xor r9, r9
			mov r9b, [rdi + rcx]
			sub r9b, '0'  	;r9b = cur digit
			add rax, r9
			inc r14
		.end_iteration:
			inc rcx
			jmp .loop
		.check_end:
			cmp r13, 1
			jnz .end	;r13==1 -> inc r14
			inc r14
	.end:
		mov rdx, r14
		pop r13
		pop r12
		pop r14
		ret

; Принимает указатель на строку (rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был).
; Незначащие нули в длину не входят!
; rdx = 0 если число прочитать не удалось.
parse_int: 
 	cmp [rdi], byte '-'
 	jz .negative	
 	.positive:
 		jmp parse_uint
 	.negative:
 		inc rdi
 		call parse_uint		; number -> rax, length -> rdx
 		inc rdx
 		neg rax
	ret 

; Принимает два указателя (rdi, rsi) на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor eax, eax	;current index
	.loop:
		mov r8b, byte [rsi + rax]	;temporary variable for compare
		cmp [rdi + rax], r8b
		jnz .bad  ;difference in strings -> .bad
		cmp [rdi + rax], byte 0
		jz .good  ;ends of strings -> .good
		inc rax
		jmp .loop
	.bad:
		xor eax, eax
		jmp .end
	.good:
		mov rax, 1
	.end:
		ret
	
; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (нуль-байт входит в эту длину) (rdx)
; Копирует строку в буфер.
; Возвращает длину строки (rax) если она умещается в буфер, иначе 0.
string_copy:
	xor rcx, rcx  ;cur index
	.loop: 
		cmp rcx, rdx
		jz .bad
		xor r9, r9
		mov r9b, [rdi + rcx]
		mov [rsi + rcx], r9b
		cmp [rdi + rcx], byte 0
		jz .good
		inc rcx
		jmp .loop
	.bad:
		xor eax, eax
		jmp .end
	.good:
		mov rax, rcx
	.end:
		xor eax, eax
		ret
