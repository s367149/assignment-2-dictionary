from subprocess import Popen, PIPE

INPUT_STRINGS = [
	"first word",
	"second word", 
	"third word",
	"fourth word",
	"fourh word",
	"fifth word",
	"last word",
	"word",
	".",
	",.",
	"",
	"1",
	"2",
	"11111000011111100001111110000111111000011111100001",
	"a" * 993,
	"1000993986979972965958951944937930923916909902895888881874867860853846839832825818811804797790783776769762755748741734727720713706699692685678671664657650643636629622615608601594587580573566559552545538531524517510503496489482475468461454447440433426419412405398391384377370363356349342335328321314307300293286279272265258251244237230223216209202195188181174167160153146139132125118111104979083766962554841342720136",
	"ssssssssssssssssssssssssssssssssssssssssss",
	"s" * 254,
	"s" * 255,
	"s" * 256, 
	"s" * 257,
	" ",
	"    ",
	"\t",
	"\t \t",
	"\t\t sss \t\t"
]

COUNT_TESTS = len(INPUT_STRINGS)

DICTIONARY = {
	"first word": "first word explanation",
	"second word": "second word explanation",
	"third word": "third word explanation",
	"fourth word": "fourth word explanation",
	"last word": "ssssssssssssssssssssssssssssssssssssssssssssssssssssss"
}

WITH_FULL_INFORMATION = False


def check(input_string):
	# пустая строчка при отсутствии ключа
	return DICTIONARY.get(input_string, "")


def print_test_data(stdin, stdout, stderr):
	print(f"stdin: '{stdin}'")
	print(f"stdout: '{stdout}'")
	print(f"stderr: '{stderr}'")


if __name__ == "__main__":
	print("Tests are starting!!!\n")

	command = './program'
	count_wrong_answers = 0
	for input_string in INPUT_STRINGS:
		proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf-8')
		stdout, stderr = proc.communicate(input=input_string + "\n")
		right_stdout = check(input_string)
		
		if stdout != right_stdout:
			count_wrong_answers += 1
			print("Wrong answer:")
			print_test_data(input_string, stdout, stderr)
			print(f"expected stdout: '{right_stdout}'")
			print('-' * 50)
		elif WITH_FULL_INFORMATION:
			print("Answer is right")
			print_test_data(input_string, stdout, stderr)
			print('-' * 50)
			
	print("\nTests are finished!")
	if count_wrong_answers == 0:
		print(f"OK ({COUNT_TESTS}/{COUNT_TESTS})")
	else:
		print(f"Wrong answers count: {count_wrong_answers}")
		print(f"Total tests count: {COUNT_TESTS}")
		exit(1)		# ненулевой код выхода, потому что не все тесты пройдены

