global find_word
section .text

%include "lib.inc"

; Принимает указатель на нуль-терминированную строку 
; (указатель на ключ или псевдо-ключ) (rdi)
; и указатель на начало словаря (rsi).
; Проходит по всему словарю в поисках подходящего ключа.  
; Если подходящее вхождение найдено, вернёт (rax) адрес начала 
; вхождения в словарь (не значения), иначе вернёт 0.
find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi				; cur key in dictionary
	xor eax, eax
	
	.handle_key:
		mov rdi, r12
		mov rsi, r13
		call string_equals		; -> rax
		test rax, rax			; rax != 0 -> .good
		jnz .good
		
		mov rdi, r13
		call string_length		; -> rax
		add r13, rax	
		inc r13					; key
		
		cmp qword [r13], 0		; end of dictionary -> .bad
		jz .bad

		mov r13, qword[r13]		; pointer for next key
	
		jmp .handle_key
		
	.good:
		mov rax, r13
		jmp .end	
	.bad:
		xor eax, eax
	.end:
		pop r13
		pop r12
		ret
