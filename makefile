ASM=nasm
ASMFLAGS=-f elf64
PYTHON=python3

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o dict.o dict.asm

main.o: main.asm dict.o
	$(ASM) $(ASMFLAGS) -o main.o main.asm
	
program: lib.o dict.o main.o
	ld -o program lib.o dict.o main.o
	
.PHONY: clean
clean:
	rm *.o program
	
.PHONY: test
test: program
	$(PYTHON) test.py
